package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystemDamage;

public final class AttackSubsystemImpl implements AttackSubsystemDamage {

	String name;

	PositiveInteger powergridRequirments;

	PositiveInteger capacitorConsumption;

	PositiveInteger optimalSpeed;

	PositiveInteger optimalSize;

	PositiveInteger baseDamage;

	public AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.powergridRequirments = powergridRequirments;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.isEmpty() || name.matches("^[ \t]+$")) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		int damage = 0;

		if ((target.getCurrentSpeed().value() > 0 && target.getCurrentSpeed().value() > this.optimalSpeed.value())
				|| (target.getSize().value() > 0 && target.getSize().value() < this.optimalSize.value())) {
			damage = this.baseDamage.value() / 2;
		}
		else {
			var sizeReductionModifier = target.getSize().value() >= this.optimalSize.value() ? 1
					: target.getSize().value() / this.optimalSize.value();

			var speedReductionModifier = target.getCurrentSpeed().value() <= optimalSpeed.value() ? 1
					: optimalSpeed.value() / (2 * target.getCurrentSpeed().value());

			damage = (int) Math.ceil(baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier));
		}
		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getDamage() {
		return baseDamage;
	}

}
