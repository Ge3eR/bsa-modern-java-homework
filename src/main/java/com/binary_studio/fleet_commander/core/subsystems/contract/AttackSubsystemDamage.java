package com.binary_studio.fleet_commander.core.subsystems.contract;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;

public interface AttackSubsystemDamage extends AttackSubsystem {

	PositiveInteger getDamage();

}
