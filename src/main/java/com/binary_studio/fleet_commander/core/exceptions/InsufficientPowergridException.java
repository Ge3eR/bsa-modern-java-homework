package com.binary_studio.fleet_commander.core.exceptions;

@SuppressWarnings("serial")
public final class InsufficientPowergridException extends Exception {

	public static final String POWER_GRID_CAPACITY = "Should not fit over power grid capacity";

	public static final String POWER_GRID_CAPACITY_REFIT = "Should not re-fit module over capacity";

	public InsufficientPowergridException(Integer missingPowergrid) {
		super(String.format("Missing %d MW to fit this module", missingPowergrid));
	}

	public InsufficientPowergridException(String missingText) {
		super(String.format("%s", missingText));
	}

}
