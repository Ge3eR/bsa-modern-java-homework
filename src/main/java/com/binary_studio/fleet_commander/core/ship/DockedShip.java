package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ExtendedModularVessel;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ExtendedModularVessel {

	final PositiveInteger capacitorOriginalAmount;

	final PositiveInteger capacitorOriginalRechargeRate;

	final PositiveInteger shieldOriginalHP;

	final PositiveInteger hullOriginalHP;

	String name;

	PositiveInteger shieldHP;

	PositiveInteger hullHP;

	PositiveInteger powergridOutput;

	PositiveInteger capacitorAmount;

	PositiveInteger capacitorRechargeRate;

	PositiveInteger speed;

	PositiveInteger size;

	protected AttackSubsystem attackSubsystem;

	protected DefenciveSubsystem defenciveSubsystem;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.shieldOriginalHP = shieldHP;
		this.hullOriginalHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorOriginalAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.capacitorOriginalRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;

	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			attackSubsystem = null;
			return;
		}

		if (this.powergridOutput.value() < subsystem.getPowerGridConsumption().value()) {
			throw new InsufficientPowergridException(
					Math.abs(this.powergridOutput.value() - subsystem.getPowerGridConsumption().value()));
		}
		attackSubsystem = subsystem;
		this.powergridOutput = PositiveInteger
				.of(this.powergridOutput.value() - subsystem.getPowerGridConsumption().value());
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			defenciveSubsystem = null;
			return;
		}

		if (this.powergridOutput.value() < subsystem.getPowerGridConsumption().value()) {
			throw new InsufficientPowergridException(
					Math.abs(this.powergridOutput.value() - subsystem.getPowerGridConsumption().value()));
		}
		defenciveSubsystem = subsystem;
		this.powergridOutput = PositiveInteger
				.of(this.powergridOutput.value() - subsystem.getPowerGridConsumption().value());

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (attackSubsystem == null && defenciveSubsystem == null)
			throw NotAllSubsystemsFitted.undockMissing();
		if (attackSubsystem == null)
			throw NotAllSubsystemsFitted.undockAttackMissing();
		if (defenciveSubsystem == null)
			throw NotAllSubsystemsFitted.undockDefenciveMissing();

		return new CombatReadyShip(this);
	}

	public AttackSubsystem getAttackSubsystemInstance() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystemInstance() {
		return this.defenciveSubsystem;
	}

}
