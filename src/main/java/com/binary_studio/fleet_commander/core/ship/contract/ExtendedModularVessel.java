package com.binary_studio.fleet_commander.core.ship.contract;

import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public interface ExtendedModularVessel extends ModularVessel {

	AttackSubsystem getAttackSubsystemInstance();

	DefenciveSubsystem getDefenciveSubsystemInstance();

}
