package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	DockedShip ship;

	public CombatReadyShip(DockedShip ship) {
		this.ship = ship;
	}

	@Override
	public void endTurn() {
		this.capacitorRegenerator();
	}

	public void capacitorRegenerator() {
		ship.capacitorAmount = PositiveInteger
				.of(Math.min(ship.capacitorAmount.value() + ship.capacitorRechargeRate.value(),
						ship.capacitorOriginalAmount.value()));
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return ship.name;
	}

	@Override
	public PositiveInteger getSize() {
		return ship.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return ship.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (ship.capacitorAmount.value() < ship.getAttackSubsystemInstance().getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		var attack = ship.attackSubsystem.attack(target);
		ship.capacitorAmount = PositiveInteger.of(Math.max(0,
				ship.capacitorAmount.value() - ship.getAttackSubsystemInstance().getCapacitorConsumption().value()));

		return Optional.of(new AttackAction(attack, this, target, ship.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {

		PositiveInteger damage = PositiveInteger
				.of(ship.getDefenciveSubsystemInstance().reduceDamage(attack).damage.value());

		if (attack.target == this) {
			int hp = ship.shieldHP.value() - damage.value();
			ship.shieldHP = PositiveInteger.of(Math.max(hp, 0));
			if (ship.shieldHP.value() == 0) {
				hp = ship.hullHP.value() + hp;
				ship.hullHP = PositiveInteger.of(Math.max(hp, 0));
			}
		}

		if (this.ship.hullHP.value() == 0) {
			return new AttackResult.Destroyed();
		}
		return new AttackResult.DamageRecived(attack.weapon, damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (ship.capacitorAmount.value() < ship.getDefenciveSubsystemInstance().getCapacitorConsumption().value()) {
			return Optional.empty();
		}

		final var regenerateAction = ship.defenciveSubsystem.regenerate();
		PositiveInteger shieldHP = PositiveInteger.of(0);
		PositiveInteger hullHP = PositiveInteger.of(0);

		if (ship.shieldHP.value() < ship.shieldOriginalHP.value()) {
			shieldHP = PositiveInteger.of(regenerateAction.shieldHPRegenerated.value());
		}
		else if (ship.hullHP.value() < ship.hullOriginalHP.value()) {
			hullHP = PositiveInteger.of(regenerateAction.hullHPRegenerated.value());
		}
		ship.capacitorAmount = PositiveInteger.of(Math.max(0,
				ship.capacitorAmount.value() - ship.getDefenciveSubsystemInstance().getCapacitorConsumption().value()));

		return Optional
				.of(new RegenerateAction(PositiveInteger.of(Math.min(ship.shieldOriginalHP.value(), shieldHP.value())),
						PositiveInteger.of(Math.min(ship.hullOriginalHP.value(), hullHP.value()))));
	}

}
