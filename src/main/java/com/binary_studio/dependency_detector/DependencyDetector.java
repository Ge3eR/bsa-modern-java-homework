package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class DependencyDetector {

	private final static int NOT_VISITED = 0;

	private final static int VISITED = 1;

	private final static int RESOLVED = 2;

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		var deps = new HashMap<String, List<String>>();
		for (var dep : libraries.dependencies) {
			if (!deps.containsKey(dep[0])) {
				deps.put(dep[0], new ArrayList<>());
			}
			deps.get(dep[0]).add(dep[1]);
		}

		var resolutionStatus = new HashMap<String, Integer>();

		for (var dep : libraries.libraries) {
			if (!checkCanResolve(deps, dep, resolutionStatus)) {
				return false;
			}
		}

		return true;
	}

	private static boolean checkCanResolve(Map<String, List<String>> deps, String lib,
			Map<String, Integer> resolutionStatus) {
		var status = resolutionStatus.getOrDefault(lib, NOT_VISITED);

		if (status == VISITED) {
			return false;
		}

		if (status == RESOLVED) {
			return true;
		}

		resolutionStatus.put(lib, VISITED);

		for (var dep : deps.getOrDefault(lib, List.of())) {
			if (!checkCanResolve(deps, dep, resolutionStatus)) {
				return false;
			}
		}
		resolutionStatus.put(lib, RESOLVED);

		return true;
	}

}