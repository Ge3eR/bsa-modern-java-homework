package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	static class AcademyCoinAccumulator {

		public final int profit;

		public final Integer val;

		private AcademyCoinAccumulator(int profit, Integer val) {
			this.profit = profit;
			this.val = val;
		}

		public static AcademyCoinAccumulator empty() {
			return new AcademyCoinAccumulator(0, Integer.MAX_VALUE);
		}

		public static AcademyCoinAccumulator of(int price) {
			return new AcademyCoinAccumulator(0, price);
		}

		public static AcademyCoinAccumulator concat(AcademyCoinAccumulator a, AcademyCoinAccumulator b) {
			return new AcademyCoinAccumulator(a.profit + Math.max(b.val - a.val, 0), b.val);
		}

	}

	public static int maxProfit(Stream<Integer> prices) {
		return prices.map(AcademyCoinAccumulator::of).reduce(AcademyCoinAccumulator.empty(),
				AcademyCoinAccumulator::concat).profit;
	}

}

/*
 * package com.binary_studio.academy_coin;
 *
 * import java.util.ArrayList; import java.util.Arrays; import
 * java.util.stream.Collectors; import java.util.stream.IntStream; import
 * java.util.stream.Stream; import java.util.Stack;
 *
 * public final class AcademyCoin {
 *
 * // private static Stack stack = new Stack(); private static final int COIN_BUY = 0;
 * private static final int COIN_SELL = 1; private static volatile int switcher =
 * COIN_BUY; private static int lastIndex = 0;
 *
 * public static void main(String[] args) {
 * System.out.println(AcademyCoin.maxProfit(Arrays.stream(new int[]{ 7, 1, 5, 3, 6, 4
 * }).boxed())); }
 *
 *
 * private AcademyCoin() { }
 *
 *
 * public static int maxProfit(Stream<Integer> prices) {
 *
 * // var pricesList = prices.filter(integer -> integer > 0).collect(Collectors.toList());
 * var intArray = prices.filter(i -> i > 0).mapToInt(Integer::intValue).toArray();
 *
 * int collect = 0; int lastBuyPrice = 0; for(int i = 0; i < intArray.length; i++) {
 *
 * switch (switcher) { case COIN_BUY: // 1. находим следующее минимальное значение в
 * списке // var min = (prices.min((o1, o2) -> o1).get());
 *
 * // 2. покупаем его lastBuyPrice = findMin(intArray, lastIndex); collect -=
 * lastBuyPrice; i = lastIndex;
 *
 * //if(i == intArray.length-1) return 0; // oops продавать некуда
 *
 * // меняем полюса switcher = COIN_SELL; System.out.println("buy " + findMin(intArray,
 * lastIndex) + "lastIndex " + lastIndex); break; case COIN_SELL: // 3. находим следующее
 * максимальное значение в списке // var max = (prices.min((o1, o2) -> o1).get()); // 4.
 * продаём по нему collect += findMax(intArray, lastIndex); i = lastIndex; // меняем
 * полюса switcher = COIN_BUY;
 *
 * System.out.println("sell " + findMax(intArray, lastIndex) + "lastIndex " + lastIndex);
 * break; } }
 *
 *
 * return collect;
 *
 * }
 *
 * private static int findMin(int[] array, int offset) { int value = Integer.MAX_VALUE;
 * int i = offset; for (; i < array.length; i++) { if (array[i] < value) { value =
 * array[i]; lastIndex = i; } } return value; }
 *
 * private static int findMax(int[] array, int offset) { int value = Integer.MIN_VALUE;
 * int i = offset; for (; i < array.length; i++) { if (array[i] > value) { value =
 * array[i]; lastIndex = i; } } return value; }
 *
 * }
 */
